\documentclass[10pt, usenames, dvipsnames]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{xcolor} % Pretty colors
\usepackage{tikz}
\usetikzlibrary{calc}
%\usepackage{pgfplots}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[noend]{algorithm2e}
\usepackage{booktabs}
\usepackage{MyMaccros}

\newcommand\Tr{{\color{ForestGreen}T}}
\newcommand\Fa{{\color{Red}F}}

% These lines replace `else' and `do' in algorithms with `:' (shorter)
\SetKwIF{If}{ElseIf}{Else}{if}{:}{else if}{else}{endif}
\SetKwFor{For}{for}{:}{endfor}
\SetKwFor{While}{while}{:}{endw}
\DontPrintSemicolon{}

% A light gray color for background
\definecolor{NotTooWhite}{RGB}{220,220,220}

% Defining styles
\setbeamertemplate{blocks}[rounded][shadow=false]
\setbeamercolor{structure}{fg=Brown}
\setbeamercolor{background canvas}{bg=NotTooWhite}
\setbeamertemplate{footline}{\hspace*{\fill}\insertframenumber/\inserttotalframenumber\hspace*{0.3cm}}
\setbeamerfont{footline}{series=\bfseries,size=\fontsize{8}{12}\selectfont}
\setbeamertemplate{navigation symbols}{}

% For not numbering backup slides
\newcommand{\beginbackup}{
   \newcounter{framenumbervorappendix}
   \setcounter{framenumbervorappendix}{\value{framenumber}}
}
\newcommand{\backupend}{
   \addtocounter{framenumbervorappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumbervorappendix}} 
}

% fix spacing when writing math equation inside a block
\newcommand{\blockmathspacefix}{
  \vspace*{-\baselineskip}\setlength\belowdisplayshortskip{0pt}
}

\AtBeginSection[]
{
\begin{frame}<beamer>{Table of Contents}
\tableofcontents[currentsection,
    sectionstyle=show/shaded
]
\end{frame}
}

% Defining Tikz styles
% \tikzset{mynode/.style = []}
% \tikzset{metanode/.style = {fill, Brown!15, rounded corners}}
\tikzset{legendnode/.style = {Brown, thick, font=\bfseries}}
\tikzset{separation/.style = {NotTooWhite}}
% \tikzset{DTSstate/.style = {}}
% \tikzset{DTSedge/.style = {->, thick}}
% \tikzset{DTSlabel/.style = {font=\scriptsize}}
\tikzset{GAstate/.style = {Blue, circle, fill = Blue!20, minimum size = 0.8cm}}
\tikzset{GAedge/.style = {->, thick, Red}}
% \tikzset{posetnode/.style = {fill, black, circle}}
% \tikzset{posetedge/.style = {black}}

\title{Guard Automata to Verify Distributed Algorithms}
\author{Bastien Thomas\inst{1}, Nathalie Bertrand\inst{1}, Josef Widder\inst{2}}
\institute{\inst{1} Univ Rennes, Inria, CNRS, IRISA, France \and \inst{2} Informal Systems, Austria}
\date{}

\begin{document}

\begin{frame}
  \maketitle
\end{frame}


\begin{frame}{Threshold-Based Distributed Algorithms}
  \begin{algorithm}[H]
    \small
    \SetKwProg{Fn}{Process}{:}{}
    \Fn{$\mathrm{DummyPhaseKing}\footnotemark{}(n, t, \mathrm{id}, v)$}{
      \KwData{
        $n$ processes,
        $t < \frac{n}{4}$ Byzantine faults,
        $\mathrm{id} \in \{0 \dotso n-1\}$,
        $v \in \{0, 1\}$.
      }
      \For%(\tcc*[h]{$t+1$ iterations})
      {$\lay = 0$ to $t$}{
        broadcast $(\lay, \mathrm{id}, v)$\;
        receive all the messages $(\lay, \_, \_)$\;
        \uIf{$\# \text{ of $(\lay, \_, 0)$ received} > \frac{n}{2} + t$}{$v \leftarrow 0$
        }\uElseIf{$\# \text{ of $(\lay, \_, 1)$ received} > \frac{n}{2} + t$}{
          $v \leftarrow 1$
        }\uElse{
          $v \leftarrow v'$ where $(\lay, \lay, v')$ is a received message\;
        }
      }
    }%
  \end{algorithm}%
  \begin{block}{Characteristics}%
    \begin{description}[Non deterministic:]
    \item[Parametrised:] Arbitrary number of processes
    \item[Non deterministic:] Caused here by Byzantine faults
    \item[Asynchronous:] Can also handle synchronous cases
    \item[\alert{Unbounded}:] \alert{Arbitrary or infinite number of rounds}
    \end{description}
  \end{block}

  \footnotetext[1]{Inspired by: P. Berman and J. A. Garay. Cloture votes: n/4-resilient distributed consensus in t+1 rounds. \textit{Mathematical Systems Theory, 1993}}
\end{frame}

\begin{frame}{Abstraction Steps}
  \resizebox{\textwidth}{!}{
    \begin{tikzpicture}

      \newcommand\xstart{6.5}
      \newcommand\dx{0.9}
      \newcommand\ystart{-5}
      \newcommand\dy{0.8}
      
      \newcommand\xstartb{6.5}
      \newcommand\ystartb{0}
      
      \node (Full) at (0, 0) {
        \(
        \begin{array}{r|r|ccccc}
          \multicolumn{7}{c}{\text{Full Configuration}} \\
          \midrule
          \DTSstate & \DTSp_0 & v_0 & k_1 & v_1 & \cdot & \cdot\\
                    & \DTSp_1 & v_1 & v_1 & k_1 & v_1 & \cdot \\
                    & \DTSp_2 & v_1 & v_0 & v_1 & \cdot & \cdot \\
          \midrule
          \DTSreceived(\DTSp_0) & \DTSp_0 & v_0 & k_1 & v_1  & \cdot & \cdot    \\
                    & \DTSp_1 & v_1 & v_1 & \cdot & \cdot & \cdot \\
                    & \DTSp_2 & v_1 & v_0  & \cdot & \cdot & \cdot    \\
          \DTSreceived(\DTSp_1) & \cdots & \multicolumn{5}{c}{\cdots} \\
          \DTSreceived(\DTSp_2) & \cdots & \multicolumn{5}{c}{\cdots} \\
          \bottomrule
        \end{array}
        \)
      };

      \onslide<2->{
        \node (Simpl) at (8, 2) {
          \(
          \begin{array}{r|ccccc}
            \multicolumn{6}{c}{\text{Succinct Configuration}} \\
            \midrule
            \DTSp_0 & v_0 & k_1 & v_1 & \cdot & \cdot   \\
            \DTSp_1 & v_1 & v_1 & k_1 & v_1 & \cdot\\
            \DTSp_2 & v_1 & v_0 & v_1 & \cdot &\cdot   \\
            \bottomrule
          \end{array}
          \)
        };

        \node[Red] at (8, {\ystartb+0.9*\dy}) {$\approx$};
        
        \foreach \num in {-0.5, 0.5, 1.5, 2.5, 3.5}{
          \draw[thin, gray] (\xstartb - \dx, \ystartb -\num*\dy) -- (\xstartb + 4.5*\dx, \ystartb -\num*\dy);
        }
        \foreach \layer in {0,1,2,3}{
          \foreach \num/\content in {
            0/$v_0$,
            1/$k_0$,
            2/$v_1$,
            3/$k_1$%
          }{
            \node (X\layer\num) at (\xstartb + \layer*\dx, \ystartb - \num*\dy) {\content};
          }
        }
        \foreach \num in {0,1,2,3}{
          \node (X4\num) at (\xstartb + 4*\dx, \ystartb - \num*\dy) {$\cdots$};
        }
        \node[Blue, thick] (P0) at ({\xstartb - 0.6*\dx}, \ystartb) {$p_0$:};
        \draw[Blue, thick] (X00) -- (X13) -- (X22);
        \node[ForestGreen, thick] (P1) at ({\xstartb - 0.6*\dx}, \ystartb-2*\dy) {$p_1$:};
        \draw[ForestGreen, thick] (X02) -- (X12) -- (X23) -- (X32);
        \node[BurntOrange, thick] (P2) at ({\xstartb - 1*\dx}, \ystartb-2*\dy) {$p_2$};
        \draw[BurntOrange, thick] (X02) -- (X10) -- (X22);

        \draw[dashed] let \p1 = (Simpl.north) in
        ({\xstartb - 1.3*\dx}, {\ystartb - 3.7*\dy}) rectangle ({\xstartb +4.7*\dx}, {\y1});
      }

      \onslide<3->{
        \node (Count) at (\xstart + 1.5*\dx, \ystart+\dy) {Counter Configuration, $n=4$, $t=1$, $f=1$};
        \foreach \num / \name in {
          0 / $v_0:$,
          1 / $k_0:$,
          2 / $v_1:$,
          3 / $k_1:$
        }{
          \node at (\xstart - 0.6*\dx, \ystart - \num*\dy) {\name};
        }
        \foreach \num in {-0.5, 0.5, 1.5, 2.5, 3.5}{
          \draw[thin, gray] (\xstart - \dx, \ystart -\num*\dy) -- (\xstart + 4.5*\dx, \ystart -\num*\dy);
        }
        \foreach \layer / \num / \content in {
          0/0/1,
          0/1/0,
          0/2/2,
          0/3/0,
          1/0/1,
          1/1/0,
          1/2/1,
          1/3/1,
          2/0/0,
          2/1/0,
          2/2/2,
          2/3/1,
          3/0/0,
          3/1/0,
          3/2/1,
          3/3/0,
          4/0/\cdots,
          4/1/\cdots,
          4/2/\cdots,
          4/3/\cdots%
        }{
          \node (S\layer\num) at (\xstart + \layer*\dx, \ystart - \num*\dy) {$\content$};
        }
        \foreach \src / \dest in {
          S00/S13,
          S02/S12,
          S02/S10,
          S13/S22,
          S12/S23,
          S10/S22,
          S23/S32%
        }{
          \draw[thick, Blue, ->] (\src) -> (\dest);
        }
      }

      \onslide<4>{
        \node (Guard) at (0, -5.5) {
          \(
          \begin{array}{r|ccccc}
            \multicolumn{5}{c}{\text{Guard Configuration}} \\
            \midrule
            v_0 > 0                          & \Tr & \Tr & \Fa & \Fa & \cdots \\
            k_0 > 0                          & \Fa & \Fa & \Fa & \Fa & \cdots \\
            v_1 > 0                          & \Tr & \Tr & \Tr & \Tr & \cdots \\
            k_1 > 0                          & \Fa & \Tr & \Tr & \Fa & \cdots \\
            2(v_0 + k_0 + f) > n + 2t        & \Fa & \Fa & \Fa & \Fa & \cdots \\
            2(v_1 + k_1 + f) > n + 2t        & \Fa & \Fa & \Tr & \Fa & \cdots \\
            2(v_0 + k_0) > n + 2t            & \Fa & \Fa & \Fa & \Fa & \cdots \\
            2(v_1 + k_1) > n + 2t            & \Fa & \Fa & \Fa & \Fa & \cdots \\
            v_0 + k_0 + v_1 + k_1 + f \geq n & \Tr & \Tr & \Tr & \Fa & \cdots \\
            \bottomrule
          \end{array}
          \)
        };
      }
      \onslide<2->{
        \draw[->, very thick, Red] let \p1 = (Full.07) in
        (\x1, \y1) -- ({\xstartb - 1.3*\dx}, \y1);
        
        \draw[->, thick, dashed, Blue] let \p1 = (Full.353) in
        ({\xstartb - 1.3*\dx}, \y1) -- (\x1, \y1);
      }
      
      \onslide<3->{
        \draw[->, very thick, Red] ({\xstartb + \dx}, {\ystartb - 3.7*\dy}) --
        ({\xstartb + \dx}, {\ystart +1.3*\dy});
      
        \draw[->, thick, dashed, Blue] ({\xstartb + 2*\dx}, {\ystart +1.3*\dy}) -- ({\xstartb + 2*\dx}, {\ystartb - 3.7*\dy});
      }
    
      \onslide<4>{
        \draw[->, very thick, Red] (Guard.east -| Count.west) -- (Guard.east);
      }
    \end{tikzpicture}
  }
\end{frame}

\begin{frame}{Reachable Configurations}

  \begin{block}{Counter-configuration}
    \centering%
    \begin{tikzpicture}[xscale=1.8, yscale=0.8]
      \foreach \x in {0,1,...,4}{
        \node[legendnode] (L\x) at (\x, 3.8) {$\x$};
      }
      \node[legendnode] (L4) at (4.7, 3.8) {$\cdots$};
      \foreach \y in {0,1,2,3}{
        \node (V5\y) at (4.7, \y) {$\cdots$};
      }
      \draw[separation] (-0.6, 3.5) -- (5, 3.5);

        \foreach \y/\lab in {
          0/$k_0:$,
          1/$v_0:$,
          2/$v_1:$,
          3/$k_1:$%
        }{
          \node[legendnode] (SL\y) at (-0.4, \y) {\lab};
        }

        \foreach \x/\ka/\kb/\kc/\kd in {
          0/$0$/$2$/$1$/$1$,
          1/$1$/$2$/$1$/$0$,
          2/$1$/$3$/$0$/$0$,
          3/$0$/$4$/$0$/$0$,
          4/$0$/$2$/$0$/$0$%
        }{
          \foreach \y/\k in {
            0/\ka,
            1/\kb,
            2/\kc,
            3/\kc%
          }{
            \node (V\x\y) at (\x, \y) {\k};
          }
        }

        \foreach \src/\dest in {
          (V03)/(V12),
          (V02)/(V11),
          (V01)/(V11),
          (V01)/(V10),
          (V12)/(V21),
          (V11)/(V21),
          (V11)/(V20),
          (V10)/(V21),
          (V20)/(V31)%
        }{
          \draw[thick, Blue] \src -- \dest;
        }

        \draw[thick, Blue] (V21) edge node[above] {\small{$\times 3$}} (V31);
        \draw[thick, Blue] (V31) edge node[above] {\small{$\times 2$}} (V41);
        
    \end{tikzpicture}
  \end{block}

  \begin{block}{Theorem}
    A counter-configuration is reachable iff it is both:
    \begin{description}%[Guard-coherent:]
    \item[Flow-coherent:] No more processes exit a state than enter it.
    \item[Guard-coherent:] If a transition is taken, then its guard is satisfied.
    \end{description}
  \end{block}
  \pause%
  This theorem uses \emph{Domain Theory} to handle configurations reachable with an \alert{infinite number of steps}.
\end{frame}

\begin{frame}{Abstraction Steps}
  \resizebox{\textwidth}{!}{
    \begin{tikzpicture}

      \newcommand\xstart{6.5}
      \newcommand\dx{0.9}
      \newcommand\ystart{-5}
      \newcommand\dy{0.8}
      
      \newcommand\xstartb{6.5}
      \newcommand\ystartb{0}
      
      \node (Full) at (0, 0) {
        \(
        \begin{array}{r|r|ccccc}
          \multicolumn{7}{c}{\text{Full Configuration}} \\
          \midrule
          \DTSstate & \DTSp_0 & v_0 & k_1 & v_1 & \cdot & \cdot\\
                    & \DTSp_1 & v_1 & v_1 & k_1 & v_1 & \cdot \\
                    & \DTSp_2 & v_1 & v_0 & v_1 & \cdot & \cdot \\
          \midrule
          \DTSreceived(\DTSp_0) & \DTSp_0 & v_0 & k_1 & v_1  & \cdot & \cdot    \\
                    & \DTSp_1 & v_1 & v_1 & \cdot & \cdot & \cdot \\
                    & \DTSp_2 & v_1 & v_0  & \cdot & \cdot & \cdot    \\
          \DTSreceived(\DTSp_1) & \cdots & \multicolumn{5}{c}{\cdots} \\
          \DTSreceived(\DTSp_2) & \cdots & \multicolumn{5}{c}{\cdots} \\
          \bottomrule
        \end{array}
        \)
      };

        \node (Simpl) at (8, 2) {
          \(
          \begin{array}{r|ccccc}
            \multicolumn{6}{c}{\text{Succinct Configuration}} \\
            \midrule
            \DTSp_0 & v_0 & k_1 & v_1 & \cdot & \cdot   \\
            \DTSp_1 & v_1 & v_1 & k_1 & v_1 & \cdot\\
            \DTSp_2 & v_1 & v_0 & v_1 & \cdot &\cdot   \\
            \bottomrule
          \end{array}
          \)
        };

        \node[Red] at (8, {\ystartb+0.9*\dy}) {$\approx$};
        
        \foreach \num in {-0.5, 0.5, 1.5, 2.5, 3.5}{
          \draw[thin, gray] (\xstartb - \dx, \ystartb -\num*\dy) -- (\xstartb + 4.5*\dx, \ystartb -\num*\dy);
        }
        \foreach \layer in {0,1,2,3}{
          \foreach \num/\content in {
            0/$v_0$,
            1/$k_0$,
            2/$v_1$,
            3/$k_1$%
          }{
            \node (X\layer\num) at (\xstartb + \layer*\dx, \ystartb - \num*\dy) {\content};
          }
        }
        \foreach \num in {0,1,2,3}{
          \node (X4\num) at (\xstartb + 4*\dx, \ystartb - \num*\dy) {$\cdots$};
        }
        \node[Blue, thick] (P0) at ({\xstartb - 0.6*\dx}, \ystartb) {$p_0$:};
        \draw[Blue, thick] (X00) -- (X13) -- (X22);
        \node[ForestGreen, thick] (P1) at ({\xstartb - 0.6*\dx}, \ystartb-2*\dy) {$p_1$:};
        \draw[ForestGreen, thick] (X02) -- (X12) -- (X23) -- (X32);
        \node[BurntOrange, thick] (P2) at ({\xstartb - 1*\dx}, \ystartb-2*\dy) {$p_2$};
        \draw[BurntOrange, thick] (X02) -- (X10) -- (X22);

        \draw[dashed] let \p1 = (Simpl.north) in
        ({\xstartb - 1.3*\dx}, {\ystartb - 3.7*\dy}) rectangle ({\xstartb +4.7*\dx}, {\y1});

        \node (Count) at (\xstart + 1.5*\dx, \ystart+\dy) {Counter Configuration, $n=4$, $t=1$, $f=1$};
        \foreach \num / \name in {
          0 / $v_0:$,
          1 / $k_0:$,
          2 / $v_1:$,
          3 / $k_1:$
        }{
          \node at (\xstart - 0.6*\dx, \ystart - \num*\dy) {\name};
        }
        \foreach \num in {-0.5, 0.5, 1.5, 2.5, 3.5}{
          \draw[thin, gray] (\xstart - \dx, \ystart -\num*\dy) -- (\xstart + 4.5*\dx, \ystart -\num*\dy);
        }
        \foreach \layer / \num / \content in {
          0/0/1,
          0/1/0,
          0/2/2,
          0/3/0,
          1/0/1,
          1/1/0,
          1/2/1,
          1/3/1,
          2/0/0,
          2/1/0,
          2/2/2,
          2/3/1,
          3/0/0,
          3/1/0,
          3/2/1,
          3/3/0,
          4/0/\cdots,
          4/1/\cdots,
          4/2/\cdots,
          4/3/\cdots%
        }{
          \node (S\layer\num) at (\xstart + \layer*\dx, \ystart - \num*\dy) {$\content$};
        }
        \foreach \src / \dest in {
          S00/S13,
          S02/S12,
          S02/S10,
          S13/S22,
          S12/S23,
          S10/S22,
          S23/S32%
        }{
          \draw[thick, Blue, ->] (\src) -> (\dest);
        }

        \node (Guard) at (0, -5.5) {
          \(
          \begin{array}{r|ccccc}
            \multicolumn{5}{c}{\text{Guard Configuration}} \\
            \midrule
            v_0 > 0                          & \Tr & \Tr & \Fa & \Fa & \cdots \\
            k_0 > 0                          & \Fa & \Fa & \Fa & \Fa & \cdots \\
            v_1 > 0                          & \Tr & \Tr & \Tr & \Tr & \cdots \\
            k_1 > 0                          & \Fa & \Tr & \Tr & \Fa & \cdots \\
            2(v_0 + k_0 + f) > n + 2t        & \Fa & \Fa & \Fa & \Fa & \cdots \\
            2(v_1 + k_1 + f) > n + 2t        & \Fa & \Fa & \Tr & \Fa & \cdots \\
            2(v_0 + k_0) > n + 2t            & \Fa & \Fa & \Fa & \Fa & \cdots \\
            2(v_1 + k_1) > n + 2t            & \Fa & \Fa & \Fa & \Fa & \cdots \\
            v_0 + k_0 + v_1 + k_1 + f \geq n & \Tr & \Tr & \Tr & \Fa & \cdots \\
            \bottomrule
          \end{array}
          \)
        };
        \draw[->, very thick, Red] let \p1 = (Full.07) in
        (\x1, \y1) -- ({\xstartb - 1.3*\dx}, \y1);
        
        \draw[->, thick, dashed, Blue] let \p1 = (Full.353) in
        ({\xstartb - 1.3*\dx}, \y1) -- (\x1, \y1);
      
        \draw[->, very thick, Red] ({\xstartb + \dx}, {\ystartb - 3.7*\dy}) --
        ({\xstartb + \dx}, {\ystart +1.3*\dy});
      
        \draw[->, thick, dashed, Blue] ({\xstartb + 2*\dx}, {\ystart +1.3*\dy}) -- ({\xstartb + 2*\dx}, {\ystartb - 3.7*\dy});
    
        \draw[->, very thick, Red] (Guard.east -| Count.west) -- (Guard.east);
    \end{tikzpicture}
  }
\end{frame}

\begin{frame}{Guard Automaton of Phase King Algorithm}

  \begin{block}{Simplified Automaton of Infinite Configurations}
    \centering
    \begin{tikzpicture}[yscale=0.5, xscale=0.64]
      \foreach \y/\gu in {
        5/$k_0 > 0$,
        4/$k_1 > 0$,
        3/$v_0 > 0$,
        2/$v_1 > 0$,
        1/$2(v_0 + f) > n + 2t$,
        0/$2(v_1 + f) > n + 2t$,
      }{
        \node[legendnode] at (-2.8, \y) {\gu};
      }

      \foreach \x/\y in {
        0/5,
        0/4,
        0/3,
        0/1,
        1/5,
        1/3,
        1/1,
        2/5,
        2/1,
        2/0,
        3/5,
        3/1,
        4/5,
        4/4,
        4/1,
        5/5,
        5/4,
        5/1,
        5/0,
        6/5,
        6/4,
        6/0,
        7/4,
        7/1,
        7/0,
        8/4,
        8/0,
        9/4,
        9/2,
        9/0,
        10/5,
        10/4,
        10/2,
        10/0%
      }{
        \node (V\x\y) at (\x, \y) {$\cdot$};
      }

      \foreach \x/\y in {
        0/2,
        0/0,
        1/4,
        1/2,
        1/0,
        2/4,
        2/3,
        2/2,
        3/4,
        3/3,
        3/2,
        3/0,
        4/3,
        4/2,
        4/0,
        5/3,
        5/2,
        6/3,
        6/2,
        6/1,
        7/5,
        7/3,
        7/2,
        8/5,
        8/3,
        8/2,
        8/1,
        9/5,
        9/3,
        9/1,
        10/3,
        10/1%
      }{
        \node (V\x\y) at (\x, \y) {$\Tr$};
      }

      \fill[fill=Blue!20, fill opacity = 0.60] (-0.4, -0.4) rectangle (1.4, 5.4);
      \fill[fill=Blue!20, fill opacity = 0.60] (1.6, -0.4) rectangle (3.4, 5.4);
      \fill[fill=Blue!20, fill opacity = 0.60] (3.6, -0.4) rectangle (6.4, 5.4);
      \fill[fill=Blue!20, fill opacity = 0.60] (6.6, -0.4) rectangle (8.4, 5.4);
      \fill[fill=Blue!20, fill opacity = 0.60] (8.6, -0.4) rectangle (10.4, 5.4);

      \draw[->, thick, Red] (0.1, 5.4) .. controls (-0.5, 6.5) and (0.9, 6.5) .. (0.3, 5.4);
      \draw[->, thick, Red] (4.9, 5.4) .. controls (4.3, 6.5) and (5.7, 6.5) .. (5.1, 5.4);
      \draw[->, thick, Red] (9.7, 5.4) .. controls (9.1, 6.5) and (10.5, 6.5) .. (9.9, 5.4);
      \path[->, thick, Red] (2.1, 5.4) edge[bend right] (0.9, 5.4);
      \path[->, thick, Red] (4.7, 5.4) edge[bend right] (0.5, 5.4);
      \path[->, thick, Red] (4.3, 5.4) edge[bend right] (2.7, 5.4);
      \path[->, thick, Red] (7.9, 5.4) edge[bend left] (9.1, 5.4);
      \path[->, thick, Red] (5.3, 5.4) edge[bend left] (9.5, 5.4);
      \path[->, thick, Red] (5.7, 5.4) edge[bend left] (7.3, 5.4);
      
    \end{tikzpicture}
  \end{block}
  \begin{block}{Liveness}
    If no process get stuck, then:
    \begin{itemize}
    \item Either no non-bysantine King is ever chosen ($k_0$ and $k_1 = 0$)
    \item If $k_0$ or $k_1 > 0$, then at the next layer ($v_0$ or $v_1 = 0$)
    \end{itemize}
  \end{block}
  \alert{More complex properties can also be checked this way}
\end{frame}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
