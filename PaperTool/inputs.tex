%\section{The PyLTA Tool}
\section{Input Format and Usage}%
\label{sec: inputs}
The input format is based on layered threshold automata (LTA) defined in \cite{BTW-concur21},
which we illustrate on the running example.
% A \emph{layered Threshold automaton} (LTA) defines a set of
% configurations that result from executions of an algorithm. PyLTA has
% a dedicated input language to define LTAs. Below, we illustrate
% how the LTA of Algorithm~\ref{alg: phase king} is defined. 
An input file needs to define three elements: \emph{parameters},
\emph{states} and \emph{guards}.

%\paragraph{Parameters} 
In PyLTA, the set of parameters are declared as follows.
\begin{small}
\begin{verbatim}
PARAMETERS: n, t, f
PARAMETER_RELATION: 4*t < n
\end{verbatim}
\end{small}
The second line declares a constraint on these
parameters, here $4t < n$, which is a necessary condition
  for the correctness of Algorithm~\ref{alg: phase
    king}. %In fact, most distributed algorithms assume such constraints on the parameters.
% This corresponds to the constraint $4t<n$ of Algorithm~\ref{alg: phase king} which is a necessary condition for the correctness of the algorithm.


As in our running example, the input format assumes that the states of the considered systems
belong to layers.
The following line defines two consecutive layers
\verb|A|, \verb|B|, and specifies after layer~$\verb|B|$, we come back to layer $\verb|A|$ and loop.
\begin{small}
\begin{verbatim}
LAYERS: A, B, A
\end{verbatim}
\end{small}
In other terms, this results in the sequence of layers
\verb|A, B, A, B,...|.
One can also specify lasso-shaped sequences; for instance, 
\verb|LAYERS: A, B, B| would yield the sequence \verb|A, B, B, B, ...|.

States can be declared by specifying the name of the layer and the name of the state
separated by a period as below.
\begin{small}
  \begin{verbatim}
STATES: A.0, A.1
STATES: B.k0, B.0, B.u, B.1, B.k1
\end{verbatim}
\end{small}
For instance, the first line defines the states $a_0$ and $a_1$ in Figure~\ref{fig: config
  phase king}, and the second line is the rest of the states.

Transitions are defined by distinguishing cases
% \todo{this expression
%   bothers me for some reason, we cannot (and often should not) use an
%   else branch here, it implies some determinism that is not present.}
for each state using guards.  In
Algorithm~\ref{alg: phase king}, a process needs to receive more than
$\frac{n}{2} + t$ messages $(2i, 1)$ in order to move from state $a_1$
(line~\ref{line: phase king a}) to $b_1$ (line~\ref{line: phase king
  b1}).  These messages can either come from processes in state $a_1$
or from Byzantine processes. In PyLTA, this condition is called the
\emph{guard} from $a_1$ to $b_1$ and it is expressed with the formula
$2(a_1 + f) > n + 2t$.  State names correspond to
the number of correct processes that have been at that state, so
transitions are declared as follows.
% In the configuration of
% Figure~\ref{fig: config phase king}, this guard does not hold in layer
% $\lay = 0$ because $2(2 + 1) > 5 + 2\times 1$ is $\false$, but it does
% hold in layer $\lay = 3$ where $a_1$ takes the value $3$.
\begin{small}
\begin{verbatim}
FORMULA Afull: A.0 + A.1 + f == n
CASE A.1:
  IF Afull & 2*(A.1 + f) >= n THEN B.k1
  IF Afull & 2*(A.1 + f) >= n + 2*t THEN B.1
...
\end{verbatim}
\end{small}
The formula \verb|Afull| is used to enforce synchrony: no process can
take a transition before every message was received.  We present the
other transitions for Algorithm~\ref{alg: phase king} in
Table~\ref{table:transitions}. Note that \verb|Afull| or an equivalent
\verb|Bfull| should also be added each time in order to avoid
considering asynchronous executions.
\begin{table}[b]
  \caption{The guards of the transitions for Algorithm~\ref{alg: phase king}. The table on the left is for transitions leaving states
  of layers $\ell=2i$, and the table on the right is for those with layer~$\ell=2i+1$. Each cell is the guard of the transition
  from the state of the row to the state of the column.}
  \label{table:transitions}    
  {\small
    \begin{tabular}{c|c|c|c|c|c|} \cline{2-6}
      $\lay = 2i$ & $k_0$ & $b_0$ & $b_?$ & $b_1$ & $k_1$ \\ \hline
      \multicolumn{1}{|c|}{$a_0$} &
      $2(a_0 + f)$ &
      $2(a_0 + f)$ &
      $2 a_0 \leq n + 2t$ &
      $2(a_1 + f)$ &
      $2(a_1 + f)$ \\ \cline{1-1}
      \multicolumn{1}{|c|}{$a_1$} &
      $\geq n$ &
      $\geq n + 2t$ &
      $\wedge 2 a_1 \leq n + 2t$ &
      $\geq n + 2t$ &
      $\geq n$ \\ \hline
    \end{tabular}
  }
  {\small
    \begin{tabular}{c|c|c|} \cline{2-3}
      $\lay = 2i + 1$ & $a_0$ & $a_1$ \\ \hline
      \multicolumn{1}{|c|}{$k_0$} & \multirow{2}{*}{$\true$} & \multirow{2}{*}{$\false$} \\ \cline{1-1}
      \multicolumn{1}{|c|}{$b_0$} & & \\ \hline
      \multicolumn{1}{|c|}{$b_?$} & $k_1 = 0$ & $k_0 = 0$ \\ \hline
      \multicolumn{1}{|c|}{$b_1$} & \multirow{2}{*}{$\false$} & \multirow{2}{*}{$\true$} \\ \cline{1-1}
      \multicolumn{1}{|c|}{$k_1$} & & \\ \hline
    \end{tabular}
  }
\end{table}

The following instruction is used to declare an LTL specification
to be verified on the configurations:
\begin{small}
\begin{verbatim}
WITH
  A.initial: A.0 + A.1 + f == n
  A.one0: A.0 > 0
  B.not_two_kings: B.k0 + B.k1 <= 1
VERIFY: (A.initial & ! A.one0 & G(B -> B.not_two_kings)) -> G(A -> ! A.one0)
\end{verbatim}
\end{small}
The instructions between \verb|WITH| and \verb|VERIFY| define
predicates at given layers, which can be used in the subsequent LTL
formula.  Here, \verb|A.one0| holds when at least one process is in
state \verb|A.0|; and \verb|B.not_two_kings| is used to prevent
executions where more than one king is present in a round. These
predicates can then be used as propositions of the LTL formula that
will be verified.

A layer type name (\verb|A| or \verb|B|) inside a formula indicates a
predicate that only holds in the corresponding layers. An
interpretation of the formula can therefore be the following: ``if
there are \verb|n| processes, and no process in \verb|A.0|, and there
is always at most one non-Byzantine king in layers of type \verb|B|,
then at all layers of type \verb|A|, there is no process in
\verb|A.0|.''

% For convenience, we also allow the following type of instructions:
% \begin{verbatim}
% TERM Ball: B.k0 + B.0 + B.u + B.1 + B.k1
% FORMULA Bfull: Ball + f == n
% \end{verbatim}
% These can be used to bind terms and formulas to identifiers in order
% to be used in later guards, predicates or verification goals.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
