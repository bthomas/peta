import Formulas as fm
from LTA import CLTA
from Paths import Checker
from GuardAutomaton import GuardAutomaton

lta = CLTA()

# Parameters
n = lta.add_parameter("n")
t = lta.add_parameter("t")
f = lta.add_parameter("f")

# Parameter Constraint
lta.set_parameter_constraint(fm.And(
    fm.lt(4*t, n),
    fm.le(f, t),
    fm.gt(n, 1)
))

# States and layer
layer = lta.append_layer(["k0", "v0", "v1", "k1"])
k0 = layer["k0"]
v0 = layer["v0"]
v1 = layer["v1"]
k1 = layer["k1"]

# Guards
for src in [k0, v0, v1, k1]: # The guards only depends on the destination
    for dest in [k0, v0]:
        majority0 = fm.gt(2*(v0 + k0 + f), n + 2*t) # more than n/2+t processes chose 0
        king0 = fm.And(
            fm.ge(2*(v0 + k0), n + 2*t),
            fm.le(2*(v1 + k1), n + 2*t),
            fm.eq(k1, 0)
        )
        lta.set_guard(src, dest,
            fm.And(
                fm.eq(k0 + v0 + v1 + k1 + f, n), # Inforces Synchronicity
                fm.Or(majority0, king0)
            )
        )

    for dest in [k1, v1]:
        majority1 = fm.gt(2*(v1 + k1 + f), n + 2*t) # more than n/2+t processes chose 1
        king1 = fm.And(
            fm.le(2*(v1 + k1), n + 2*t),
            fm.le(2*(v0 + k0), n + 2*t),
            fm.eq(k0, 0)
        )
        lta.set_guard(src, dest,
            fm.And(
                fm.eq(k0 + v0 + v1 + k1 + f, n), # Inforces Synchronicity
                fm.Or(majority1, king1)
            )
        )

# layer_constraint
lta.set_layer_constraint(0,
    fm.And(
        fm.le(k0 + k1, 1), # At most one king
        fm.le(k0 + v0 + v1 + k1 + f, n)
    )
)

# Propositions
lta.add_proposition(0, "k0 > 0", fm.gt(k0, 0))
lta.add_proposition(0, "k1 > 0", fm.gt(k1, 0))
lta.add_proposition(0, "v0 > 0", fm.gt(v0, 0))
lta.add_proposition(0, "v1 > 0", fm.gt(v1, 0))
lta.add_proposition(0, "layer full", fm.ge(k0 + v0 + v1 + k1 + f, n))
lta.add_proposition(0, "majority 0", fm.gt(2*(k0 + v0 + f), n + 2*t))
lta.add_proposition(0, "majority 1", fm.gt(2*(k1 + v1 + f), n + 2*t))
lta.add_proposition(0, "strong majority 0", fm.gt(2*(k0 + v0), n + 2*t))
lta.add_proposition(0, "strong majority 1", fm.gt(2*(k1 + v1), n + 2*t))

# Bounded checking 5 layers
# checker = Checker(lta, 0, 5)
# val = checker.get_empty_val()
# val[0, "layer full"] = True
# val[4, "layer full"] = True
# print("Bounded Non triviality, expect True: {}".format(checker.check(val)[0]))

# val = checker.get_empty_val()
# val[0, "layer full"] = True
# val[0, "strong majority 0"] = True
# val[4, "v1 > 0"] = True
# print("Bounded Safety, expect False: {}".format(checker.check(val)))

# Test GA
ga = GuardAutomaton(lta)
ini = ga.initial_state
constraints = {"layer full": True}

print("computing state list...")
initials = ga.right_extentions(ini, constraints)

def successors(st):
    edges = ga.right_extentions(st, constraints)
    return {ga.pop_left(s) for s in edges}

stack = list(initials.copy())
visited_states = set()
while stack:
    s = stack.pop()
    if s not in visited_states:
        visited_states.add(s)
        succ = successors(s)
        stack += list(succ)

state_list = list(visited_states)
print(ga.print_val_states(0, state_list))
result = ""
max_len_state = len(str(len(state_list) - 1))
for i, state in enumerate(state_list):
    succ = successors(state)
    index_succ = [state_list.index(s) for s in succ]
    index_succ.sort()
    padded_succ = [" " * (max_len_state - len(str(s))) + str(s) for s in index_succ]
    result += "Successors({}){}: ".format(str(i), " " * (max_len_state - len(str(i))))
    result += ", ".join(padded_succ)
    result += "\n"
print(result)

filtered = [
    s for s in visited_states if
    s.valuation[(0, "k0 > 0")] or s.valuation[(0, "k1 > 0")]
]
filtered_succs = set()
for s in filtered:
    filtered_succs.update(successors(s))
live = True
for s in filtered_succs:
    if s.valuation[(0, "v0 > 0")] and s.valuation[(0, "v1 > 0")]:
        live = False

print("Liveness: {}".format(live))

