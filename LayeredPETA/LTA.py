import Formulas as fm

class LayeringError(Exception):
    """ Exception raised when some instructions do not satisfy the layering hypothesis """

class State(fm.Var):
    """ Represent the states of an LTA. Can be built by the
    LTA itself. Should be hashable.
    """
    def __init__(self, layer, name):
        self.layer = layer
        fm.Var.__init__(self, "s({})".format(name))

    def __hash__(self):
        return hash((self.layer, self.name))

    def __eq__(self, other):
        if isinstance(other, State):
            return (self.layer == other.layer) and (self.name == other.name)
        return False

class Parameter(fm.Var):
    """ Represent the parameters of an LTA. Can be built by the
    LTA itself. Overwrites the to_z3 method to keep the parameters
    constants.
    """
    layer = None

    def __init__(self, name):
        fm.Var.__init__(self, "p({})".format(name))

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        if isinstance(other, Parameter):
            return self.name == other.name
        return False

    def to_z3(self, prefix=""):
        """ Return a z3 Integer Variable. For these variables the prefix
        parameter is ignored.
        """
        return fm.Var.to_z3(self, prefix="")

class Edge(fm.Var):
    """ Used for formulas that requires the value of an edge """
    def __init__(self, layer, source, destination):
        self.layer = layer
        name = "e({}, {})".format(source.name, destination.name)
        fm.Var.__init__(self, name)

    def __hash__(self):
        return hash((self.layer, self.name))

    def __eq__(self, other):
        if isinstance(other, Edge):
            return (self.layer == other.layer) and (self.name == other.name)
        return False

class FLTA:
    """ A class used to construct finite LTAs """
    def __init__(self):
        """ Create an empty LTA, use methods add_parameter, add_layer and
        add_guard to build a more complex one. """
        self.guards = [] # guard[layer][source][destination] will be a formula. Default to false
        self.parameters = set()
        self.parameter_constraint = fm.true # restricts the parameter space
        self.propositions = [] # propositions defined at each layers
        self.layer_constraint = [] # restrict the layer space

    def num_layers(self):
        """ Return the number of layers of the LTA """
        return len(self.guards)

    def get_parameters(self):
        """ Return the list of parameters of the LTA """
        return self.parameters

    def get_parameter_constraint(self):
        """ Return the constraint defined on the parameters """
        return self.parameter_constraint

    def get_states(self, layer):
        """ Return the list of states of the input layer """
        return self.guards[layer].keys()

    def get_propositions(self, layer):
        """ Return a dictionary associating each proposition index to the
        corresponding proposition."""
        return self.propositions[layer]

    def get_constraint(self, layer):
        """ Return the constraint defined on the input layer """
        return self.layer_constraint[layer]

    def get_guard(self, source, destination):
        """ Return the guard(source, destination).
        source and destination should belong to consecutive layers.
        """
        layer = source.layer
        return self.guards[layer][source].get(destination, fm.false)

    def get_successors(self, state):
        """ Returns the list of states s for which guard(state, s) is not false """
        layer = state.layer
        return self.guards[layer][state].keys()

    def add_parameter(self, name):
        """ Add a new named parameter to the LTA """
        res = Parameter(name)
        self.parameters.add(res)
        return res

    def set_parameter_constraint(self, formula):
        """ Declare a formula to restrict the parameter space """
        var_set = formula.var_set()
        assert var_set.issubset(self.parameters)
        self.parameter_constraint = formula

    def append_layer(self, state_names):
        """ Add an additional layer to the automaton.
        state_names should contain an iterable of unique
        names for the states.
        Return a dictionary containing the states indexed by their names.
        """
        num_layers = self.num_layers()
        self.guards.append({State(num_layers, name): {} for name in state_names})
        self.propositions.append({})
        self.layer_constraint.append(fm.true)
        return {name: State(num_layers, name) for name in state_names}

    def next_layer(self, layer):
        """ Return the index of the next layer of the automaton """
        if layer < len(self.guards):
            return layer + 1
        raise LayeringError("Attempted to reach beyond the last layer")

    @staticmethod
    def is_formula_layer(formula, layer):
        """
        Return True if the input formula contains only parameter variable and
        states variable from the input layer
        """
        var_set = formula.var_set()
        for var in var_set:
            if not (var.layer is None or var.layer == layer):
                return False
        return True

    def set_guard(self, source, destination, formula):
        """ Set guard(source, destination) = formula.
        source and destination should belong to consecutive layers.
        """
        layer = source.layer
        next_layer = self.next_layer(layer)
        if not self.is_formula_layer(formula, layer):
            raise LayeringError("Formula does not constraint source layer")
        if source not in self.guards[layer].keys():
            raise LayeringError("Source state was not recorded in the automaton")
        if destination not in self.guards[next_layer].keys():
            raise LayeringError("Destination state was not recorded in the automaton")
        self.guards[layer][source][destination] = formula

    def set_layer_constraint(self, layer, formula):
        """ Set a constraint on the given layer that are imposed to be true """
        if not self.is_formula_layer(formula, layer):
            raise LayeringError("Formula does not constraint layer")
        self.layer_constraint[layer] = formula

    def add_proposition(self, layer, name, formula):
        """ Add an observable proposition to the lta at the given layer, it can
        be referred to later by its name
        """
        if not self.is_formula_layer(formula, layer):
            raise LayeringError("Formula does not constraint layer")
        self.propositions[layer][name] = formula

    def edge_variable(self, source, destination):
        """ Return a variable corresponding to the edge between
        source and destination """
        layer = source.layer
        if destination.layer != self.next_layer(layer):
            raise LayeringError("Edge is between non-successive layers")
        return Edge(layer, source, destination)

class CLTA(FLTA):
    """ A class used to construct cyclic LTAs """

    def next_layer(self, layer):
        """ Return the index of the next layer of the automaton,
        wrap back to 0 if the last layer is given """
        return (layer + 1) % len(self.guards)

    def get_states(self, layer):
        """ Return the list of states of the input layer """
        return self.guards[layer % len(self.guards)].keys()

    def get_propositions(self, layer):
        """ Return a dictionary associating each proposition index to the
        corresponding proposition."""
        return self.propositions[layer % len(self.guards)]

    def get_constraint(self, layer):
        """ Return the constraint defined on the input layer """
        return self.layer_constraint[layer % len(self.guards)]

    def get_successors(self, state):
        """ Returns the list of states s for which guard(state, s) is not false """
        layer = state.layer % len(self.guards)
        return self.guards[layer][state].keys()
