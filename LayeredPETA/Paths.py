import z3

class Checker:
    """ A class used to check the existence of finite paths in an LTA. """

    def __init__(self, lta, starting_layer, length):
        """ Initialises a Checker object. It will check for paths of
        given length that starts in starting_layer """
        self.lta = lta
        self.start = starting_layer
        self.length = length
        self.solver = z3.Solver()

        self.init_parameters()
        for layer in range(starting_layer, starting_layer + length):
            self.init_layer(layer)
            self.init_edges_src(layer)

        for layer in range(starting_layer, starting_layer + length - 1):
            self.init_edges_dest(layer)

    def init_parameters(self):
        """ Add the conditions on the parameters to the solver """
        for param in self.lta.get_parameters():
            self.solver.add(param.to_z3() >= 0)
        self.solver.add(self.lta.get_parameter_constraint().to_z3())

    @staticmethod
    def get_prefix(layer):
        """ Defines the prefix that identifies the different layers """
        return "L{}_".format(layer)

    def prop_handle(self, layer, prop_name):
        """ Returns a z3 boolean variable that is used to force prop_name to hold """
        prefix = self.get_prefix(layer)
        handle_name = "{}g({})".format(prefix, prop_name)
        return z3.Bool(handle_name)

    def init_layer(self, layer):
        """ Add the constraints relating to the given layer to the solver """
        prefix = self.get_prefix(layer)
        for state in self.lta.get_states(layer):
            var_s = state.to_z3(prefix=prefix)
            self.solver.add(var_s >= 0)
        self.solver.add(self.lta.get_constraint(layer).to_z3(prefix=prefix))
        for name, prop in self.lta.get_propositions(layer).items():
            z3_prop = prop.to_z3(prefix=prefix)
            prop_handle = self.prop_handle(layer, name)
            self.solver.add(prop_handle == z3_prop)

    def init_edges_src(self, source_layer):
        """ Initialises the conditions relating the edges and their source """
        src_prefix = self.get_prefix(source_layer)

        # src >= sum(leaving edges)
        for src in self.lta.get_states(source_layer):
            succ = self.lta.get_successors(src)
            leaving_edges = [self.lta.edge_variable(src, dest) for dest in succ]
            sum_leaving = sum([e.to_z3(prefix=src_prefix) for e in leaving_edges])
            self.solver.add(src.to_z3(prefix=src_prefix) >= sum_leaving)

        # guard satisfied \/ edge == 0
        for src in self.lta.get_states(source_layer):
            for dest in self.lta.get_successors(src):
                edge = self.lta.edge_variable(src, dest)
                guard = self.lta.get_guard(src, dest)
                z3_e = edge.to_z3(prefix=src_prefix)
                z3_guard = guard.to_z3(prefix=src_prefix)
                self.solver.add(z3_e >= 0)
                self.solver.add(z3.Or(z3_guard, z3_e == 0))

    def init_edges_dest(self, source_layer):
        """ Initialises the conditions relating the edges and their destination """
        src_prefix = self.get_prefix(source_layer)
        dest_prefix = self.get_prefix(source_layer + 1)

        # sum(entering edges) == dest
        pred_array = {dest: [] for dest in self.lta.get_states(source_layer + 1)}
        for src in self.lta.get_states(source_layer):
            for dest in self.lta.get_successors(src):
                pred_array[dest].append(src)
        for dest, pred in pred_array.items():
            entering_edges = [self.lta.edge_variable(src, dest) for src in pred]
            sum_entering = sum([e.to_z3(prefix=src_prefix) for e in entering_edges])
            self.solver.add(sum_entering == dest.to_z3(prefix=dest_prefix))

    def get_empty_val(self):
        """ Return an empty valuation of the propositions. """
        return {}

    def check(self, val_prop):
        """
        Check if the valuation of the proposition val_prop is satisfiable.
        If it is, the first output is true and the second is an extended
        valuation that is satisfiable.
        If it is not, the first output is false and the second is a smaller
        valuation that is not satisfiable.
        """
        constraints = []
        for (layer, prop_name), val in val_prop.items():
            assert prop_name in self.lta.get_propositions(layer)
            if val:
                constraints.append(self.prop_handle(layer, prop_name))
            else:
                constraints.append(z3.Not(self.prop_handle(layer, prop_name)))
        is_sat = self.solver.check(constraints)
        if is_sat == z3.sat:
            model = self.solver.model()
            res = self.get_empty_val()
            for layer in range(self.start, self.start+self.length):
                for name, prop in self.lta.get_propositions(layer).items():
                    res[(layer, name)] = z3.is_true(model[self.prop_handle(layer, name)])
            return(True, res)
        if is_sat == z3.unsat:
            core = self.solver.unsat_core()
            res = self.get_empty_val()
            for layer in range(self.start, self.start+self.length):
                for name, prop in self.lta.get_propositions(layer).items():
                    prop_handle = self.prop_handle(layer, name)
                    if prop_handle in core:
                        res[(layer, name)] = True
                    elif z3.Not(prop_handle) in core:
                        res[(layer, name)] = False
            return(False, res)
        raise RuntimeError("z3 solver output: {}".format(is_sat))
