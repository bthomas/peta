from collections import deque
from lta_encoder_ga import LTAEncoderGA
from valuation_manager import ValManager

class GuardAutomaton:
    char_true = "t"
    char_false = "f"

    def __init__(self, lta):
        self.lta = lta
        self.guard_abstraction = LTAEncoderGA(self.lta, len(lta) + 1)

        # List of propositions
        self.propositions = [ValManager() for _ in range(len(self.lta))]
        self.initial_cond = {}

    def add_proposition(self, clayer, name, prop):
        k = len(self.lta)
        for layer in range(clayer, self.guard_abstraction.length, k):
            self.guard_abstraction.add_proposition(layer, name, prop)
        self.propositions[clayer].add_proposition(name)

    def add_parametric_relation(self, formula):
        self.guard_abstraction.add_assertion(None, formula)

    def add_initial_cond(self, valuation):
        for name in valuation.keys():
            if name not in self.propositions[0].get_props():
                raise KeyError("No propositions named {} in layer 0".format(name))
        self.initial_cond = valuation

    def add_assertion(self, clayer, formula):
        k = len(self.lta)
        for layer in range(clayer, self.guard_abstraction.length, k):
            self.guard_abstraction.add_assertion(layer, formula)

    def get_state(self, clayer, val):
        """
        Return a hashable object encoding the state with the valuation val
        of propositions at clayer
        """
        val_str = self.propositions[clayer].val_to_repr(val)
        return (clayer, val_str)

    def get_state_val(self, state):
        """ Inverse function of the method get_state """
        clayer, val_str = state
        val = self.propositions[clayer].repr_to_val(val_str)
        return (clayer, val)

    def get_initial_states(self):
        partial_val = self.guard_abstraction.get_empty_valuation()
        for name, value in self.initial_cond.items():
            partial_val[0][name] = value
        complete_vals = self.guard_abstraction.complete_valuation(
            partial_val,
            start=0,
            length=1
        )
        return [self.get_state(0, val[0]) for val in complete_vals]

    def get_successors(self, state):
        clayer, val = self.get_state_val(state)
        next_clayer = self.lta.next_layer_index(clayer)
        partial_val = self.guard_abstraction.get_empty_valuation()
        for name, value in val.items():
            partial_val[clayer][name] = value
        complete_vals = self.guard_abstraction.complete_valuation(
            partial_val,
            start=clayer,
            length=2
        )
        return [self.get_state(next_clayer, val[clayer+1]) for val in complete_vals]

    def build_adjacence_list(self):
        result = {}
        queue = deque()
        # BFS instead of DFS because of clause learning
        for state in self.get_initial_states():
            queue.append(state)
        while queue:
            state = queue.popleft()
            if state not in result:
                successors = self.get_successors(state)
                for next_state in successors:
                    queue.append(next_state)
                result[state] = set(successors)
        return result
