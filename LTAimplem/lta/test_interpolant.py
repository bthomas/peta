from pysmt.shortcuts import *
from pysmt.typing import INT
from mathsat import *

s = Solver(name="msat", solver_options={"interpolation": "true"})
env = s.msat_env()
group_a = msat_create_itp_group(env)
group_b = msat_create_itp_group(env)

a0 = Symbol("a0", INT)
a1 = Symbol("a1", INT)
ac0 = Symbol("ac0", INT)
ac1 = Symbol("ac1", INT)

c = Symbol("c", INT)

res = msat_set_itp_group(env, group_a)
assert res == 0
fa = And(
    GE(a0, Int(0)),
    GE(a1, Int(0)),
    GE(ac0, Int(0)),
    GE(ac1, Int(0)),
    Equals(a0, ac0),
    Equals(a1, ac1),
    LE(a0, Int(2)),
    LE(a1, Int(2))
)
s.add_assertion(fa)

res = msat_set_itp_group(env, group_b)
assert res == 0
fb = And(
    GE(c, Int(0)),
    Equals(ac0 + ac1, c),
    #GT(c, Int(4))
)
s.add_assertion(fb)

res = msat_set_itp_group(env, group_a)
assert res == 0

if s.solve([GT(c, Int(4))]):
    model = s.get_model()
    print(model)
else:
    interpolant = msat_get_interpolant(env, [group_a])
    print(s.converter.back(interpolant))
