from enum import Enum
from parsimonious.grammar import Grammar, NodeVisitor
from lta.lta import CLTA

class Type(Enum):
    PARAMETER = 1
    LAYER = 2
    VARTYPE = 3
    VARVALUE = 4
    STATE = 5
    TERM = 6
    FORMULA = 7

class Parsor:
    def __init__(self):
        grammar_str = ""
        with open("grammar.txt", "r") as grammar_file:
            grammar_str = grammar_file.read()
        self.grammar = Grammar(grammar_str)
        
    def get_ast(self, input_string):
        return self.grammar.parse(input_string)
