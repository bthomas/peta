
from pysmt.shortcuts import Solver
from enum import Enum
import lta.formulas as fm
from lta.lta import LTA, CLTA
from lta.formulas_converter import FormulaConverter
from lta.all_sat import all_sat

class Valuation:
    def __init__(self, layer_start, layer_stop):
        self.layer_start = layer_start
        self.layer_stop = layer_stop
        self.values = [{} for _ in range(layer_start, layer_stop)]

    def set_value(self, layer, prop_name, value):
        self.values[layer - self.start_layer][prop_name] = value

    def get_value(self, layer, prop_name):
        return self.values[layer - self.start_layer][prop_name]

    def layer_range(self):
        return range(self.start_layer, self.stop_layer)
    
    def layer_items(self, layer):
        return self.values[layer - self.start_layer].items()
    
class LTAEncoder:
    def __init__(self, lta, start_layer, length):
        self.start_layer = start_layer
        self.stop_layer = start_layer + length
        self.lta = lta
        self.converter = FormulaConverter()
        self.propositions = [[] for _ in range(self.start_layer, self.stop_layer)]
        self.solver = Solver()

        param_constraints = self.lta.get_parameter_constraints()
        self.add_assertions(None, *param_constraints)
        for layer in range(self.start_layer, self.stop_layer):
            layer_index = self.lta.get_layer_index(layer)
            out_constraints = self.lta.out_constraints(layer_index)
            self.add_assertions(layer, *out_constraints)

        for layer in range(self.start_layer, self.stop_layer - 1):
            layer_index = self.lta.get_layer_index(layer)
            in_constraints = self.lta.in_constraints(layer_index)
            self.add_assertions(layer, *in_constraints)

    def __len__(self):
        return self.stop_layer - self.start_layer
    
    def add_assertions(self, layer, *constraints):
        for constraint in constraints:
            pysmt_constraint = self.converter.convert_formula(constraint, layer)
            self.solver.add_assertion(pysmt_constraint)

    def solve(self, soft_constraints=None):
        if soft_constraints is None:
            return self.solver.solve()
        pysmt_constraints = []
        for i, layer_constraints in soft_constraints:
            layer = self.start_layer + i
            pysmt_constraints += [self.converter.convert_formula(c, layer) for c in layer_constraints]
        return self.solver.solve(pysmt_constraints)

    def get_prop_handle(self, name):
        return fm.BooleanVariable("H({})".format(name))

    def get_propositions(self, layer):
        return self.propositions[layer - self.start_layer]
    
    def add_proposition(self, layer, name, proposition):
        if name in self.get_propositions(layer):
            raise KeyError("Already a proposition named{} in layer {}".format(name, layer))
        self.propositions[layer - self.start_layer].append(name)
        handle = self.get_prop_handle(name)
        prop_setup = fm.Iff(handle, proposition)
        self.add_assertions(layer, prop_setup)

    def get_empty_valuation(self):
        return Valuation(self.layer_start, self.layer_stop)

    def valuation_constraints(self, valuation):
        constraints = []
        for layer in valuation.layer_range():
            layer_constraints = []
            for prop_name, value in valuation.layer_items(layer):
                prop_handle = self.get_prop_handle(prop_name)
                if value:
                    layer_constraints.append(prop_handle)
                else:
                    layer_constraints.append(fm.Not(prop_handle))
            constraints.append(layer_assumptions)
        return constraints

    def check_valuation(self, valuation):
        soft_constraints = self.valuation_constraints(valuation)
        return self.solve(soft_constraints)

    def complete_valuation(self, valuation):
        constraints = self.valuation_constraints(valuation)
        assumptions = []
        for i, layer_constraints in enumerate(constraints):
            layer = self.start_layer + i
            assumptions += [self.converter.convert_formula(c, layer) for c in layer_constraints]
        variables = []
        for layer in range(self.start_layer, self.stop_layer):
            for prop_name in self.propositions[layer]:
                handle = self.get_prop_handle(prop_name)
                pysmt_var = self.converter.convert_boolean_variable(handle, layer)
                variables.append(pysmt_var)
        pysmt_valuations = all_sat(self.solver, variables, assumptions)
        result = []
        for pysmt_valuation in pysmt_valuations:
            valuation = self.get_empty_valuation()
            for layer in valuation.layer_range():
                for prop_name in self.propostions[layer]:
                    handle = self.get_prop_handle(prop_name)
                    pysmt_var = self.converter.convert_boolean_variable(handle, layer)
                    valuation.set_value(layer, prop_name, smt_val[smt_name])
            result.append(valuation)
        return result
