"""
This file provides the all_sat method to find all the consistant
assignment of a given set of boolean variables under smt constraints.

This problem appears in the pysmt documentation.
"""

import mathsat

def callback(model, converter, result):
    """Callback for msat_all_s at.

    This function is called by the MathSAT API everytime a new model
    is found.  If the function returns 1, the search continues,
    otherwise it stops.

    :param model: the mathsat model
    :param converter: the pysmt converter
    :param result: where mathsat should store its results

    :returns: 1 if the search should continue
    """
    # Elements in model are msat_term .
    # Converter.back() provides the pySMT representation of a solver term.
    valuation = {}
    for v in model:
        py_v = converter.back(v)
        assert py_v.is_literal()
        if py_v.is_symbol(): # Positive literal
            valuation[py_v] = True
        else:
            assert py_v.is_not()
            valuation[py_v.arg(0)] = False
    result.append(valuation)
    return 1 # go on

def all_sat(solver, variables, assertions):
    """Find all the satisfiable assignments of variables under
    assertions.

    Currently, this function only works for the mathsat solver.

    :param solver: The pysmt Solver to be used
    :param variables: The boolean variables for which assignment must
        be found
    :param assertions: additional `soft constrsaints' that are added
        to the solver during this function call alone

    :returns: a list of dictionary associating each variable of
              variables with a boolean truth value
    """
    solver.push()
    for formula in assertions:
        solver.add_assertion(formula)
    if not variables:
        # For some reasons the mathsat function crashes in this case
        # We assume here that the assertions are SAT
        if solver.solve():
            return [{}]
        return []
    result = []
    mathsat.msat_all_sat(
        solver.msat_env(),
        [solver.converter.convert(v) for v in variables],
        lambda model: callback(model, solver.converter, result)
    )
    solver.pop()
    return result
