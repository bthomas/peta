""" Takes a section of an LTA and perform the guard abstraction on it """

from pysmt.shortcuts import Symbol, Not, Implies
from pysmt.typing import BOOL

from lta_encoder import LTAEncoder
from all_sat import all_sat

class LTAEncoderGA(LTAEncoder):
    """ A version of LTAEncoder that allow to search for only sections of
    the LTA comprised between two layers. This is achieved by adding a
    boolean variable Switch at each layer and making it so that every constraints
    of the layers are guarded by this variable.
    """

    def get_layer_switch(self, layer):
        if not 0 <= layer < self.length:
            raise IndexError("Layer {} out of range".format(layer))
        name = "Switch{}".format(layer)
        return Symbol(name, BOOL)

    def guard_coherence(self, layer):
        result = super().guard_coherence(layer)
        layer_switch = self.get_layer_switch(layer)
        return Implies(layer_switch, result)

    def out_flow_coherence(self, layer):
        result = super().out_flow_coherence(layer)
        layer_switch = self.get_layer_switch(layer)
        return Implies(layer_switch, result)

    def in_flow_coherence(self, layer):
        result = super().in_flow_coherence(layer)
        layer_switch = self.get_layer_switch(layer)
        return Implies(layer_switch, result)

    def add_assertion(self, layer, formula):
        if layer is None:
            super().add_assertion(layer, formula)
            return
        if not self.formula_test(layer, formula):
            raise ValueError("{} contains unsupported variables".format(formula))
        smt_formula = self.converter.convert_formula(formula, layer)
        layer_switch = self.get_layer_switch(layer)
        self.solver.add_assertion(Implies(layer_switch, smt_formula))

    def get_proposition_setup(self, layer, name, prop):
        result = super().get_proposition_setup(layer, name, prop)
        layer_switch = self.get_layer_switch(layer)
        return Implies(layer_switch, result)

    def get_empty_valuation(self):
        """ Return an empty valuation of the propositions of the abstraction """
        return [{} for layer in range(self.length)]

    def val_to_formula(self, valuation):
        """ Utility function for converting a valuation into a pysmt clause """
        result = []
        for layer, l_valuation in enumerate(valuation):
            for name, value in l_valuation.items():
                prop_handle = self.get_prop_handle(layer, name)
                if value:
                    result.append(prop_handle)
                else:
                    result.append(Not(prop_handle))
        return result

    def check_valuation(self, valuation, start=0, length=None):
        """ Check if the input valuation is feasible or not """
        if length is None:
            length = self.length - start
        assumptions = self.val_to_formula(valuation)
        for layer in range(start, start+length):
            assumptions.append(self.get_layer_switch(layer))
        return self.solver.solve(assumptions)

    def complete_valuation(self, valuation, start=0, length=None):
        """ Given a feasible partial valuation, enumerates all the
        complete valuation that extends it. It is possible to restrict
        the querry to a sequence of layers by specifying the optional
        parameters start and length """
        if length is None:
            length = self.length - start
        # We convert the arguments to py_smt
        assumptions = self.val_to_formula(valuation)
        for layer in range(start, start+length):
            assumptions.append(self.get_layer_switch(layer))
        variables = []
        for layer in range(start, start+length):
            for name in self.propositions[layer].keys():
                variables.append(self.get_prop_handle(layer, name))
        # We get the result
        smt_valuations = all_sat(self.solver, variables, assumptions)
        # We convert the results back to our conventions
        result = []
        for smt_val in smt_valuations:
            val = self.get_empty_valuation()
            for layer in range(start, start + length):
                for name in self.propositions[layer].keys():
                    smt_name = self.get_prop_handle(layer, name)
                    val[layer][name] = smt_val[smt_name]
            result.append(val)
        return result
